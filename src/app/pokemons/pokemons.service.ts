import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Pokemon } from './pokemon';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, tap } from 'rxjs/operators';

//@ : ce service peut lui meme avoir des dépendances
@Injectable()
export class PokemonsService {

	private pokemonsUrl = 'api/pokemons';

	constructor(private http: HttpClient) { }

	/** GET pokemons */
	//cette méthode évite de répéter la boucle dans list et details component et dans 
    // Retourne tous les pokémons
	getPokemons(): Observable<Pokemon[]> {
		console.log("getPokemons all from service")
		return this.http.get<Pokemon[]>(this.pokemonsUrl).pipe(
			tap(_ => this.log(`fetched pokemons`)),
			catchError(this.handleError('getPokemons', []))
		);
	} 

	/** GET pokemon */
	    // Retourne le pokémon avec l'identifiant passé en paramètre
getPokemon(id: number): Observable<Pokemon> {
			const url = `${this.pokemonsUrl}/${id}`;
		console.log("getPokemon SOlo", "test")
			return this.http.get<Pokemon>(url).pipe(
				tap(_ => this.log(`fetched pokemon id=${id}`)),
				catchError(this.handleError<Pokemon>(`getPokemon id=${id}`))
			);
		}

	/** GET types */
	getPokemonTypes(): Array<string> {
		return ['Plante', 'Feu', 'Eau', 'Insecte', 'Normal',
			'Electrik', 'Poison', 'Fée', 'Vol', 'Combat', 'Psy'];
	}

	/** PUT pokemon */
updatePokemon(pokemon: Pokemon): Observable<any> {
		const httpOptions = {
			//type of data that is sent to the server
			headers: new HttpHeaders({ 'Content-Type': 'application/json' })
		};
			//url-data-options
		return this.http.put(this.pokemonsUrl, pokemon, httpOptions).pipe(
			tap(_ => this.log(`updated pokemon id=${pokemon.id}`)),
			catchError(this.handleError<any>('updatePokemon'))
		);
	}
	/* the Content-Type entity header is used to indicate the media type of the resource.
In responses, a Content-Type header tells the client what the content type of the returned content actually is. */

/* HTTP headers let the client and the server pass additional information with an HTTP request or response. 
	An HTTP header consists of its case-insensitive name followed by a colon (:), then by its value.

	
	/** DELETE pokemon */

	deletePokemon(pokemon: Pokemon): Observable<Pokemon> {
		const url = `${this.pokemonsUrl}/${pokemon.id}`;
		const httpOptions = {
			headers: new HttpHeaders({ 'File-Type': 'application/json' })
		};
		return this.http.delete<Pokemon>(url, httpOptions).pipe(
			tap(_ => this.log(`deleted pokemon id=${pokemon.id}`)),
			catchError(this.handleError<Pokemon>('deletePokemon'))
		);
	} 

	/* GET pokemons search */
	searchPokemons(term: string): Observable<Pokemon[]> {
		if (!term.trim()) {
			// si le terme de recherche n'existe pas, on renvoie un tableau vide.
			return of([]);
		}
		return this.http.get<Pokemon[]>(`api/pokemons/?name=${term}`).pipe(
			tap(_ => this.log(`found pokemons matching "${term}"`)),
			catchError(this.handleError<Pokemon[]>('searchPokemons', []))
		);
	}

	/* handleError */
	private handleError<T>(operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {
			console.error(error);
			console.log(`${operation} failed: ${error.message}`);

			return of(result as T);
		};
	}

	/* log */
	private log(log: string) {
		console.info(log);
	}

}

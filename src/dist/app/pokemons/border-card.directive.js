"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
//@directive est une classe !!
//element ref pour définir l'element du dom sur lequel on va plliquer notre directive
//creation d'un element directive à chaque detection de l'attribut pkmnBorderCard et injection
//de l'elemnt du DOM dans le constructeur 
var BorderCardDirective = (function () {
    function BorderCardDirective(el) {
        this.el = el;
        //propriétés 
        this.initialColor = '#f5f5f5';
        this.defaultColor = '#009688';
        this.defaultHeight = 180;
        this.setBorder(this.initialColor);
        this.setHeight(this.defaultHeight);
    }
    //@Input() pkmnBorderCard: string; //sans alias le nom est pas super adapté, 
    //utilisation du nom de la directive pour nommer la propriété
    BorderCardDirective.prototype.onMouseEnter = function () {
        //valeure par défaut si aucune couleur n'est définie
        this.setBorder(this.borderColor || this.defaultColor);
    };
    BorderCardDirective.prototype.onMouseLeave = function () {
        this.setBorder(this.initialColor);
    };
    //les méthodes qui changent les attributs
    BorderCardDirective.prototype.setBorder = function (color) {
        var border = 'solid 4px ' + color;
        this.el.nativeElement.style.border = border;
    };
    BorderCardDirective.prototype.setHeight = function (height) {
        this.el.nativeElement.style.height = height + 'px';
    };
    __decorate([
        core_1.Input('pkmnBorderCard'),
        __metadata("design:type", String)
    ], BorderCardDirective.prototype, "borderColor", void 0);
    __decorate([
        core_1.HostListener('mouseenter'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], BorderCardDirective.prototype, "onMouseEnter", null);
    __decorate([
        core_1.HostListener('mouseleave'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], BorderCardDirective.prototype, "onMouseLeave", null);
    BorderCardDirective = __decorate([
        core_1.Directive({
            //
            selector: '[pkmnBorderCard]'
        })
        //Angular crée une nouvelle instance de notre directive à chaque fois qu'il détecte 
        //un élément HTML avec l'attribut correspondant
        //la directive s'appliquera à tout les éléments du dom
        //possédant un attribut pkmnBorderCard
        //à mettre entre crochets sinon s'applique aux balises
        //il faut préfixer le nom des directives pour éviter des 
        //collisions ac ds balises html standard ou librairies tierces
        //exportation pour pouvoir utiliser dans nos composants
        ,
        __metadata("design:paramtypes", [core_1.ElementRef])
    ], BorderCardDirective);
    return BorderCardDirective;
}());
exports.BorderCardDirective = BorderCardDirective;
//# sourceMappingURL=border-card.directive.js.map